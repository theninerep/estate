=== FullPage for Gutenberg ===
Contributors: meceware, Álvaro Trigo
Tags: fullpage, full page, full screen, fullpage.js, one page, onepage, presentation, scrolling, slider, slide, slideshow, swipe, gutenberg, wordpress
Requires at least: 5.1.0
Tested up to: 5.6.1
Requires PHP: 5.6.20
Stable Tag: 1.9.0
License: Commercial

This plugin simplifies creation of fullscreen scrolling websites with WordPress and saves you big time.

== Description ==

This plugin simplifies creation of fullscreen scrolling websites with WordPress and saves you big time.

[Documentation](https://www.meceware.com/docs/fullpage-for-gutenberg/)

= Top Features =
* Fully responsive.
* Touch support for mobiles, tablets, touch screen computers.
* Easy adding sections and slides.
* Full page scroll with optional visible scrollbar.
* Optional Auto-height sections.
* CSS3 and (optional) JS animations.
* Animated anchor links.
* Optional show/hide anchor links in the address bar.
* Optional vertically centered row content.
* Optional section and slide loops.
* Optional section only scrollbars.
* Optional keyboard support while scrolling.
* Optional history record. When this is enabled, browser back button will go to the previous section.
* Optional horizontal and vertical navigation bars with different styles.
* Optional responsive scrollbar. When responsive width and height given, a normal scroll page will be used under the given width and height values.
* TEMPLATES: You can use empty page templates or supply your own page template
* CSS and JS minified.

= How To Use =

For the full documentation visit the [documentation site](https://www.meceware.com/docs/fullpage-for-gutenberg/)

* Create a new page/post.
* Add *FullPage for Gutenberg* block. There should be no other content inside the page/post.
* When the block is added, there should be 2 sections. You can add more sections or sections with slides by clicking the corresponding button at the bottom of *FullPage for Gutenberg* block.
* Add any content inside sections/slides.
* Adjust parameters of the blocks. To select the block you want to modify, click on the corresponding block title.
* To remove sections or slides, click the cross/delete icon at the top right of the block.

= Credits =

Thanks to [Álvaro Trigo](https://www.alvarotrigo.com/fullpage/) for awesome fullpage.js plugin.

== FAQ ==

No FAQ yet.

== Screenshots ==

Please check the [documentation](https://www.meceware.com/docs/fullpage-for-gutenberg/) for screenshots.

== Changelog ==

**v1.9.0**

  - Enhancement: fullpage.js library is updated to v3.1.0. (#119)
  - Enhancement: iWideo library is updated to v1.1.8.
  - Enhancement: Plugin settings page is reorganized. (#120)
  - Enhancement: Drop effect extension support is added. (#118)

**v1.8.0**

  - Enhancement: Tooltip background and text options are added to section options. (#114)
  - Enhancement: Hiding content before FullPage load is added as an option under Customization. (#115)
  - Fix: Extension key update issue is fixed. (#111)
  - Fix: Extension activated text is updated in the plugin settings page. (#113)
  - Fix: Forms button customization is added.
  - FullPage extensions file is updated to the latest.
  - English language files are added.
  - Minor bug fixes and improvements.

**v1.7.0**

  - Enhancement: Automatic extension installation and update functionality is added. Please remove the previous extensions plugin. You can check the [documentation](https://www.meceware.com/docs/fullpage-for-gutenberg/#extensions) for more details. (#63)
  - Minor improvements and package updates.

**v1.6.4**

  - Enhancement: Offset Sections extension data parameters are added. (#93)
  - Enhancement: Compatibility updates.
  - Minor improvements.

**v1.6.3**

  - Enhancement: fullpage.js library is updated to v3.0.9. (#102)
  - Enhancement: iWideo library is updated to v1.1.7. (#102)
  - Enhancement: Clickable tooltip option is added. (#100)
  - Fix: Template is reset on archive pages. (#101)
  - Minor improvements and package updates.

**v1.6.2**

  - Enhancement: iwideo is updated to v1.1.6.
  - Bug: Video parameters for new blocks bug is fixed. (#98)
  - Minor improvements.

**v1.6.1**

  - Package updates.

**v1.6.0**

  - Enhancement: Modern control arrow style is added. Option for control arrow color is added. (Control arrow are only available for slides.) (#95)
  - Enhancement: Documentation link is added to the settings page. (#85)
  - Enhancement: License key is hidden when plugin is activated. (#87)
  - Enhancement: Video keep playing option is added under Customization tab. (#94)
  - Enhancement: Footer customization is added. (#91)
  - Bug: Color buttons are fixed. (#89)
  - Bug: IE11 compatibilities.
  - Minor improvements.

**v1.5.0**

  - Enhancement: Section navigation buttons are added. (#83)
  - Enhancement: Extension loading is changed. Please check documentation for more information. (#64)
  - Enhancement: Cards extension is added.
  - Minor improvements.

**v1.4.1**

  - Enhancement: fullpage.js library is updated to v3.0.8. (#81)
  - Enhancement: Compatibility with Gutenberg 7.
  - Bug: Parallax extension compatibility is fixed. (#75)
  - Bug: Height of the videos on slides are fixed. (#69)
  - Bug: Section and slide navigation colors can be set seperately. (#80)
  - Bug: Some typos in generated CSS are fixed.
  - Minor improvements.

**v1.4.0**

  - Enhancement: Collapse button is enhanced. (#52)
  - Enhancement: FullPage section and slide title bars are redesigned. (#57)
  - Enhancement: FullPage icon is added. (#65)
  - Enhancement: Settings page is redesigned. (#68)
  - Bug: Section gets hidden when WordPress menu is opened and expand is enabled. (#66)
  - Bug: Sorting controls when expanded are reorganized. (#67)
  - Bug: Validating of number attributes are fixed. (#71)
  - Bug: Activation functionality is fixed that crashes because of the PHP version number. (#72)
  - Minor improvements.

**v1.3.0**

  - Bug: Freeze issue is fixed when Gutenberg plugin is active. (#60)
  - Enhancement: Deprecated Gutenberg functionalities are updated. (#61)
  - Enhancement: Collapse button is added to sections and slides.
  - Enhancement: Expand functionality animations are added.
  - Enhancement: Library updates.

**v1.2.2**

  - Enhancement: iwideo is updated to v1.1.2.
  - Enhancement: Library updates.

**v1.2.1**

  - Bug: Backwards compatibility.

**v1.2.0**

  - Enhancement: License deactivation is added.
  - Enhancement: Section ordering is improved.
  - Bug: Opacity + images on the edit screen is fixed.
  - Minor cosmetic improvements, text updates.

**v1.1.0**

**Note**: Please take a database backup before updating the plugin. Please contact [support](https://alvarotrigo.com/#contact) for any questions or problems.

  - Enhancement: Video backgrounds are added using iwideo library. Self-hosted videos, Youtube and Vimeo background videos can be used. Vimeo is not supported with fullpage.js so some of the options may be incompatible.
  - Enhancement: The way of selecting colors (color palette) is written from scratch. Now the colors can be selected in a more intuitive way.
  - Enhancement: Navigation color options are added to section and slide settings. Navigation bullet colors can be changed for each section and slide.
  - Enhancement: fullpage.js library is updated to v3.0.7.
  - Enhancement: afterReBuild event of fullpage.js is added.
  - Enhancement: Image opacity option is added for regular backgrounds.
  - Enhancement: Libraries are updated to latest version.
  - Code cleanup and minor improvements.

**v1.0.3**

  - License key check bug fixes and improvements. Error message is shown in more details.
  - Custom CSS is moved under Design tab.
  - Removed the license key field from Gutenberg editor.
  - Build script improvements.

**v1.0.2**

  - License key check bug fixes and improvements.

**v1.0.1**

  - Bug fixes.
  - Minor Improvements.

**v1.0.0**

  - Initial Release
