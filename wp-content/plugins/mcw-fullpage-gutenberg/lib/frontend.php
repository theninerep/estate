<?php

/* Copyright 2019-2020 Mehmet Celik */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

require_once dirname( __FILE__ ) . '/local.php';

if ( ! class_exists( 'McwFullPageFrontEnd' ) ) {
  class McwFullPageFrontEnd {
    // Tag
    private $tag = '';
    // Gutenberg block name
    private $blockName = 'meceware/fullpage';
    // Block attributes
    private $attributes = array(
      'enabled' => false,
      'isVideoChild' => false,

      // Navigation
      'navigation' => 'off',
      'navigationStyle' => 'default',

      'navigationSlide' => 'off',
      'navigationSlideStyle' => 'default',

      // Scrolling
      'scrollBar' => false,
      'scrollOverflow' => false,

      'easing' => 'css3_ease',

      // Extensions
      'enableExtensions' => false,
      'extensions' => array(),

      // Advanced Parameters
      'enableJQuery' => false,
      'enableTemplate' => true,
      'enableTemplateRedirect' => false,
      'templatePath' => '',
      'removeThemeJS' => false,
      'removeJS' => '',
    );

    // FullPageJS version
    private $fullPageVersion = '';
    // Plugin URL
    private $pluginUrl = '';
    // Block attributes are parsed or not
    private $blockAttributesReady = false;
    // Parsed block attributes
    private $blockAttributes = null;

    // Constructor
    public function __construct( $tag, $fullPageVersion, $pluginUrl ) {
      $this->tag = $tag;
      $this->fullPageVersion = $fullPageVersion;
      $this->pluginUrl = $pluginUrl;

      // WP Init action
      add_action( 'init', array( $this, 'OnInit' ) );
      // Enqueue fullpage related css and js
      add_action( 'enqueue_block_assets', array( $this, 'OnEnqueueBlockAssets' ) );

      // Template redirect
      add_action( 'template_redirect', array( $this, 'OnTemplateRedirect' ) );
      // Template include
      add_filter( 'template_include', array( $this, 'OnTemplateInclude' ) );
      // Remove unwanted JS from header
      add_action( 'wp_print_scripts', array( $this, 'OnWpPrintScripts' ) );
      // Add extra body class
      add_filter( 'body_class', array( $this, 'OnBodyClass' ) );
    }

    public function OnInit() {
      // Only load if Gutenberg is available.
      if ( ! function_exists( 'register_block_type' ) ) {
        return;
      }
      register_block_type(
        'meceware/fullpage',
        array(
          'render_callback' => array( $this, 'OnRenderCallback' ),
        )
      );
    }

    public function OnRenderCallback( $attributes, $content ) {
      // TODO: A Bug with Gutenberg and WordPress that the content is not rendered right
      // & char is encoded as &amp; at the output. Until it is fixed, this function callback is necessary.
      $content = wp_specialchars_decode( $content /* , ENT_QUOTES */ );

      // Replace extension keys
      $extensionKeys = apply_filters( 'mcw-fullpage-extension-key', array() );
      $extensionKeyHandlers = array(
        'cards' => 'cardsKey',
        'continuous-horizontal' => 'continuousHorizontalKey',
        'drag-and-move' => 'dragAndMoveKey',
        'drop-effect' => 'dropEffectKey',
        'fading-effect' => 'fadingEffectKey',
        'interlocked-slides' => 'interlockedSlidesKey',
        'offset-sections' => 'offsetSectionsKey',
        'parallax' => 'parallaxKey',
        'reset-sliders' => 'resetSlidersKey',
        'responsive-slides' => 'responsiveSlidesKey',
        'scroll-horizontally' => 'scrollHorizontallyKey',
        'scroll-overflow-reset' => 'scrollOverflowResetKey',
      );
      $replace = array();
      foreach ( $extensionKeys as $key => $value ) {
        if ( isset( $value ) && ! empty( $value ) && isset( $extensionKeyHandlers[ $key ] ) ) {
          $handler = $extensionKeyHandlers[ $key ];
          $old = $handler . ':""';
          $new = $handler . ':"' . $value . '"';
          if ( $old !== $new ) {
            $replace[ '(' . $handler . ':".*")' ] = $new;
          }
        }
      }

      if ( ! empty( $replace ) ) {
        return preg_replace( array_keys( $replace ), array_values( $replace ), $content);
      }

      return $content;
    }

    public function OnEnqueueBlockAssets() {
      // Do not enqueue on admin
      if ( is_admin() ) {
        return;
      }

      if ( ! $this->IsFullPageEnabled() ) {
        return;
      }

      // Fullpage CSS
      wp_enqueue_style( 'mcw_fp_gut_css', $this->pluginUrl . 'fullpage/fullpage.min.css', array(), $this->fullPageVersion, 'all' );

      // Section navigation CSS
      $nav = $this->GetNavigationStyle();
      if ( $nav ) {
        wp_enqueue_style( 'mcw_fp_gut_sect_nav_css', $this->pluginUrl . 'fullpage/nav/section/' . $nav . '.min.css', array( 'mcw_fp_gut_css' ), $this->fullPageVersion, 'all' );
      }

      $nav = $this->GetNavigationSlideStyle();
      if ( $nav ) {
        wp_enqueue_style( 'mcw_fp_gut_slide_nav_css', $this->pluginUrl . 'fullpage/nav/slide/' . $nav . '.min.css', array( 'mcw_fp_gut_css' ), $this->fullPageVersion, 'all' );
      }

      $dep = array();
      if ( $this->GetEnableJQuery() ) {
        $dep[] = 'jquery';
      }

      if ( $this->IsVideoUsed() ) {
        wp_enqueue_script( 'mcw_fp_iwideo', $this->pluginUrl . 'lib/assets/iwideo.min.js', $dep, '1.1.8', true );
        $dep[] = 'mcw_fp_iwideo';
      }

      // Add easing js file
      if ( substr( $this->GetEasing(), 0, 3 ) === 'js_' ) {
        wp_enqueue_script( 'mcw_fp_gut_easing_js', $this->pluginUrl . 'fullpage/vendors/easings.min.js', $dep, '1.3', true );
        $dep[] = 'mcw_fp_gut_easing_js';
      }

      // Add scrolloverflow file
      if ( $this->GetScrollOverflow() ) {
        wp_enqueue_script( 'mcw_fp_gut_iscroll_js', $this->pluginUrl . 'fullpage/vendors/scrolloverflow.min.js', $dep, '2.0.6', true );
        $dep[] = 'mcw_fp_gut_iscroll_js';
      }

      // Add filter
      if ( has_filter( 'mcw-fullpage-enqueue' ) ) {
        $dep = apply_filters( 'mcw-fullpage-enqueue', $dep, $this );
      }

      // Add fullpage JS file
      $extensions = ( ! McwFullPageCommonLocal::GetState( $this->tag ) ) || $this->GetEnableExtensions();
      $jsPath = $extensions ? 'fullpage/fullpage.extensions.min.js' : 'fullpage/fullpage.min.js';
      wp_enqueue_script( 'mcw_fp_gut_js', $this->pluginUrl . $jsPath, $dep, $this->fullPageVersion, true );
    }

    // Called by template_redirect action
    public function OnTemplateRedirect() {
      if ( ! $this->IsFullPageEnabled() ) {
        return;
      }

      if ( is_archive() ) {
        return;
      }

      $path = $this->GetTemplatePath( true );

      if ( false === $path ) {
        return;
      }

      include $path;
      exit();
    }

    // Called by template_include filter
    public function OnTemplateInclude( $template ) {
      if ( ! $this->IsFullPageEnabled() ) {
        return $template;
      }

      if ( is_archive() ) {
        return $template;
      }

      $path = $this->GetTemplatePath( false );

      if ( false === $path ) {
        return $template;
      }

      return $path;
    }

    // Remove unwanted JS from header
    // Called by wp_print_scripts action
    public function OnWpPrintScripts() {
      // Get post
      global $post;
      // Get global scripts
      global $wp_scripts;

      // Get post
      global $post;
      if ( ! ( isset( $post ) && is_object( $post ) ) ) {
        return;
      }

      // Check if fullpage is enabled
      if ( ! $this->IsFullPageEnabled() ) {
        return false;
      }

      // Check if remove theme js is enabled
      if ( $this->GetFieldValue( 'removeThemeJS' ) ) {
        // Error handling
        if ( isset( $wp_scripts ) && isset( $wp_scripts->registered ) ) {
          // Get theme URL
          $themeUrl = get_bloginfo( 'template_directory' );

          // Remove theme related scripts
          foreach ( $wp_scripts->registered as $key => $script ) {
            if ( isset( $script->src ) ) {
              if ( stristr( $script->src, $themeUrl ) !== false ) {
                // Remove theme js
                unset( $wp_scripts->registered[ $key ] );
                // Remove from queue
                if ( isset( $wp_scripts->queue ) ) {
                  $wp_scripts->queue = array_diff( $wp_scripts->queue, array( $key ) );
                  $wp_scripts->queue = array_values( $wp_scripts->queue );
                }
              }
            }
          }
        }
      }

      // Check if remove js is enabled
      $removeJS = array_filter( explode( ',', $this->GetFieldValue( 'removeJS', '' ) ) );
      if ( isset( $removeJS ) && is_array( $removeJS ) && ! empty( $removeJS ) ) {
        // Error handling
        if ( isset( $wp_scripts ) && isset( $wp_scripts->registered ) ) {
          // Remove scripts
          foreach ( $wp_scripts->registered as $key => $script ) {
            if ( isset( $script->src ) ) {
              foreach ( $removeJS as $remove ) {
                if ( ! isset( $remove ) ) {
                  continue;
                }
                // Trim js
                $remove = trim( $remove );
                // Check if script includes the removed JS
                if ( stristr( $script->src, $remove ) !== false ) {
                  // Remove js
                  unset( $wp_scripts->registered[ $key ] );
                  // Remove from queue
                  if ( isset( $wp_scripts->queue ) ) {
                    $wp_scripts->queue = array_diff( $wp_scripts->queue, array( $key ) );
                    $wp_scripts->queue = array_values( $wp_scripts->queue );
                  }
                }
              }
            }
          }
        }
      }
    }

    public function OnBodyClass( $classes ) {
      $extra = 'wp-';
      if ( ! McwFullPageCommonLocal::GetState( $this->tag ) ) {
        $classes[] = $extra . 'fullpage' . '-js';
      }
      return $classes;
    }

    private function ParseBlocks( $content ) {
      $parser_class = apply_filters( 'block_parser_class', 'WP_Block_Parser' );
      if ( class_exists( $parser_class ) ) {
        $parser = new $parser_class();
        return $parser->parse( $content );
      } elseif ( function_exists( 'parse_blocks' ) ) {
        return parse_blocks( $content );
      } elseif ( function_exists( 'gutenberg_parse_blocks' ) ) {
        return gutenberg_parse_blocks( $content );
      } else {
        return false;
      }
    }

    public function GetFieldValue( $id ) {
      // Set the default value
      $default = isset( $this->attributes[ $id ] ) ? $this->attributes[ $id ] : null;

      if ( ! $this->blockAttributesReady ) {
        // Check Gutenberg functions
        if ( ! ( function_exists( 'has_blocks' ) && has_blocks( get_the_ID() ) ) ) {
          return;
        }

        // Get post
        global $post;
        if ( ! is_object( $post ) ) {
          return $default;
        }

        $this->blockAttributesReady = true;

        // Parse blocks
        $blocks = $this->ParseBlocks( $post->post_content );
        if ( ! is_array( $blocks ) || empty( $blocks ) ) {
          return $default;
        }

        foreach ( $blocks as $indexkey => $block ) {
          if ( ! is_object( $block ) && is_array( $block ) && isset( $block['blockName'] ) ) {
            if ( $this->blockName === $block['blockName'] ) {
              if ( isset( $block['attrs'] ) && is_array( $block['attrs'] ) ) {
                $this->blockAttributes = $block['attrs'];
                break;
              }
            }
          }
        }
      }

      if ( ! is_array( $this->blockAttributes ) || empty( $this->blockAttributes ) ) {
        return $default;
      }

      $val = isset( $this->blockAttributes[ $id ] ) ? $this->blockAttributes[ $id ] : $default;

      // Add filter
      if ( has_filter( 'mcw_fp_guten_field' ) ) {
        $val = apply_filters( 'mcw_fp_guten_field', $val, $id );
      }

      // Return field value or default
      return $val;
    }

    private function GetTemplatePath( $redirect ) {
      // Get post
      global $post;
      if ( ! ( isset( $post ) && is_object( $post ) ) ) {
        return false;
      }

      // Check if fullpage is enabled
      if ( ! $this->IsFullPageEnabled() ) {
        return false;
      }

      // Check if template is enabled
      if ( ! $this->GetEnableTemplate() ) {
        return false;
      }

      if ( $redirect ) {
        // Check if template redirect is enabled
        if ( ! $this->GetEnableTemplateRedirect() ) {
          return false;
        }
      } else {
        if ( $this->GetEnableTemplateRedirect() ) {
          return false;
        }
      }

      $path = trim( $this->GetFieldValue( 'templatePath' ) );
      if ( '' === $path ) {
        $path = plugin_dir_path( dirname( __FILE__ ) ) . 'template/template.php';
      }

      // Add filter
      if ( has_filter( 'mcw_fp_template' ) ) {
        $path = apply_filters( 'mcw_fp_template', $path );
      }

      if ( empty( $path ) ) {
        return false;
      }

      return $path;
    }

    private function IsFullPageEnabled() {
      return $this->GetFieldValue( 'enabled' );
    }

    public function IsFullPageExtensionEnabled( $extension = false ) {
      if ( ! $this->IsFullPageEnabled() ) {
        return false;
      }

      if ( ! McwFullPageCommonLocal::GetState( $this->tag ) ) {
        return false;
      }

      $enabled = $this->GetEnableExtensions();

      if ( false === $extension ) {
        return $enabled;
      }

      if ( ! $enabled ) {
        return $enabled;
      }

      $extensions = $this->GetFieldValue( 'extensions' );
      if ( isset( $extensions ) && is_array( $extensions ) && isset( $extensions[0][ $extension ] ) && isset( $extensions[0][ $extension ]['enabled'] ) ) {
        return $extensions[0][ $extension ]['enabled'];
      }

      return false;
    }

    private function IsVideoUsed() {
      return $this->GetFieldValue( 'isVideoChild' );
    }

    private function GetNavigationStyle() {
      if ( $this->GetFieldValue( 'navigation' ) !== $this->attributes['navigation'] ) {
        return $this->GetFieldValue( 'navigationStyle' );
      }

      return false;
    }

    private function GetNavigationSlideStyle() {
      if ( $this->GetFieldValue( 'navigationSlide' ) !== $this->attributes['navigationSlide'] ) {
        return $this->GetFieldValue( 'navigationSlideStyle' );
      }

      return false;
    }

    private function GetEasing() {
      return $this->GetFieldValue( 'easing' );
    }

    private function GetScrollOverflow() {
      return $this->GetFieldValue( 'scrollOverflow' ) && ! $this->GetFieldValue( 'scrollBar' );
    }

    public function GetEnableExtensions() {
      return $this->GetFieldValue( 'enableExtensions' );
    }

    private function GetEnableJQuery() {
      return $this->GetFieldValue( 'enableJQuery' );
    }

    private function GetEnableTemplate() {
      return $this->GetFieldValue( 'enableTemplate' );
    }

    private function GetEnableTemplateRedirect() {
      return $this->GetFieldValue( 'enableTemplateRedirect' );
    }
  }
}
