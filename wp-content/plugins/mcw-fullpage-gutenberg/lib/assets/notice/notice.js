jQuery(document).ready(function($) {
  $('.mcw-fullpage-gutenberg-notice.is-dismissible').each(function(){
    var t = this;
    $(this).find('button.notice-dismiss').click(function(e){
      data = {
        'action': 'mcw-fullpage-gutenberg-admin-notice',
        'notice': $(t).data('notice'),
        'nonce': McwFullPageGutenberg.nonce,
      };
      $.post(McwFullPageGutenberg.ajaxurl, data);
    });
  });
});
