<?php

/**
 * Plugin Name: FullPage for Gutenberg
 * Plugin URI: https://www.meceware.com/docs/fullpage-for-gutenberg/
 * Author: Mehmet Celik, Álvaro Trigo
 * Author URI: https://www.meceware.com/
 * Version: 1.9.0
 * Description: Create beautiful scrolling fullscreen web sites with Gutenberg editor and WordPress, fast and simple. Gutenberg editor addon of FullPage JS implementation.
 * Text Domain: mcw_fullpage_gutenberg
**/

/* Copyright 2019-2020 Mehmet Celik */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

require_once dirname( __FILE__ ) . '/lib/frontend.php';
require_once dirname( __FILE__ ) . '/lib/server.php';

if ( ! class_exists( 'McwFullPageGutenberg' ) ) {

  class McwFullPageGutenberg {
    // Plugin version
    private $version = '1.9.0';
    // FullPage JS version
    private $fullPageVersion = '3.1.0';
    // Tag
    private $tag = 'mcw-fullpage-gutenberg';
    // Plugin name
    private $pluginNameBase = 'FullPage for Gutenberg';
    private $pluginName = 'FullPage for Gutenberg';
    // Server
    private $server = null;

    // Plugin settings
    private $settings = null;
    private $settingsId = '-settings';
    private $licenseKeyId = '-license-key';
    private $licenseUserMeta = '-license-user-meta';

    // Constructor
    public function __construct() {
      // Initialize object members
      $this->settingsId = $this->tag . $this->settingsId;
      $this->licenseKeyId = $this->tag . $this->licenseKeyId;
      $this->licenseUserMeta = $this->tag . $this->licenseUserMeta;

      $this->pluginName = esc_html__( $this->pluginNameBase, $this->tag );

      // Enqueue editor assets for the backend
      add_action( 'enqueue_block_editor_assets', array( $this, 'OnEnqueueBlockEditorAssets' ) );
      add_action( 'admin_enqueue_scripts', array( $this, 'onAdminEnqueueScripts' ) );

      // Create options page
      add_action( 'admin_menu', array( $this, 'onAddAdminMenu' ) );
      add_action( 'admin_init', array( $this, 'onAdminInit' ) );
      add_action( 'current_screen', array( $this, 'onCurrentScreen' ) );
      // Admin notices
      add_action( 'admin_notices', array( $this, 'onAdminNotices' ) );
      add_action( 'wp_ajax_' . $this->tag . '-admin-notice', array( $this, 'onAjaxAdminNoticeRequest' ) );

      // Create frontend script
      new McwFullPageFrontEnd( $this->tag, $this->fullPageVersion, plugins_url( '/', __FILE__ ) );
    }

    // Enqueue Gutenberg block assets for backend editor.
    public function OnEnqueueBlockEditorAssets() {
      wp_enqueue_script(
        $this->tag . '-block-js',
        plugins_url( 'dist/blocks.build.js', __FILE__ ),
        array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ),
        $this->version
      );

      wp_enqueue_style(
        $this->tag . '-block-editor-css',
        plugins_url( 'dist/blocks.editor.build.css', __FILE__ ),
        array( 'wp-edit-blocks' ),
        $this->version,
        'all'
      );

      wp_localize_script(
        $this->tag . '-block-js',
        'McwFullPageGutenbergOptions',
        array(
          'verified' => $this->getLicenseState(),
          'key' => $this->getLicenseKey(),
          'url' => menu_page_url( $this->tag, false ),
          'extensions' => $this->getExtensions(),
          'version' => $this->version,
        )
      );
    }

    public function onAdminEnqueueScripts() {
      if ( $this->getLicenseState() ) {
        return;
      }

      wp_enqueue_script(
        $this->tag . '-notice-js',
        plugins_url( 'lib/assets/notice/notice.min.js', __FILE__ ),
        array( 'jquery', 'common' ),
        false,
        true
      );

      wp_localize_script(
        $this->tag . '-notice-js',
        'McwFullPageGutenberg',
        array(
          'nonce' => wp_create_nonce( $this->tag . '-admin-notice-nonce' ),
          'ajaxurl' => admin_url( 'admin-ajax.php' ),
        )
      );
    }

    public function onAdminInit() {
      $this->server = new McwFullPageCommonServer(
        $this->pluginNameBase,
        $this->tag,
        $this->getLicenseKey(),
        $this->version,
        __FILE__
      );

      register_setting(
        $this->tag . '-group',
        $this->settingsId
      );

      // Register plugin license key setting
      add_settings_section(
        $this->tag . '-section',
        null,
        array( $this, 'onSettingsLicenseKeyRender' ),
        $this->tag . '-sections'
      );

      // Ajax request
      add_action( 'wp_ajax_fullpage_extension_install', array( $this, 'OnAjaxFullPageExtensionInstall' ) );

      do_action( $this->tag . '-admin-settings' );
    }

    public function onCurrentScreen( $screen ) {
      // Deactivate button
      if ( isset( $_POST ) && isset( $_POST['option_page'] ) && ( $_POST['option_page'] == $this->tag . '-group' ) ) {
        if ( isset( $_POST['action'] ) && ( 'update' === $_POST['action'] ) ) {
          if ( isset( $_POST['deactivate'] ) && ( $_POST['deactivate'] == esc_html__( 'Deactivate', $this->tag ) ) ) {
            $this->server->Deactivate();
            $_POST[ $this->settingsId ][ $this->licenseKeyId ] = '';
          }
        }
      }

      // Update settings
      if ( $this->isSettingsPage( $screen ) ) {
        if ( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] ) {
          $this->server->GetRemote( true, array( 'activate' => true ) );
        }
      }
    }

    public function onAddAdminMenu() {
      $optionsPage = add_options_page(
        $this->pluginName,
        $this->pluginName,
        'manage_options',
        $this->tag,
        array( $this, 'onAddOptionsPage' )
      );

      // Load settings CSS
      add_action( 'load-' . $optionsPage, array( $this, 'onAdminEnqueueSettingsStyle' ) );
    }

    public function onAdminEnqueueSettingsStyle() {
      wp_enqueue_style(
        $this->tag . '-settings',
        plugins_url( 'lib/assets/settings/settings.min.css', __FILE__ ),
        array(),
        $this->version,
        'all'
      );

      wp_enqueue_script(
        $this->tag . '-settings-js',
        plugins_url( 'lib/assets/settings/settings.min.js', __FILE__ ),
        array( 'jquery' ),
        '1.0',
        true
      );

      wp_localize_script( $this->tag . '-settings-js', 'McwFullPageSettings', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'nonce' => wp_create_nonce( $this->tag . '-fullpage-extension-install-nonce' ),
      ) );
    }

    public function onSettingsLicenseKeyRender() {
      $licenseKey = $this->getLicenseKey();
      $licenseState = $this->getLicenseState();
      $isActive = $licenseKey && $licenseState;

      $url = $isActive && current_user_can( 'update_plugins' ) && $this->server->CanUpdate() ? rawurlencode( plugin_basename( __FILE__ ) ) : false;
      if ( $url ) {
        $url = wp_nonce_url( self_admin_url( 'update.php?action=upgrade-plugin&plugin=' . $url ), 'upgrade-plugin_' . $url );
      }

      $extensions = $this->getExtensions();
      $extensionKeys = apply_filters( 'mcw-fullpage-extension-key', array() );

      foreach ( $extensions as $key => $value ) {
        $extensions[ $key ]['key'] = array_key_exists( $key, $extensionKeys ) ? $extensionKeys[ $key ] : '';
      }
      ?>

      <section class = "mcw-fp-settings-flex-sect">
        <div class = "mcw-fp-settings-row-parent">
          <div class = "mcw-fp-settings-cell">
            <div class = "mcw-fp-settings-content-wrapper mcw-fp-settings-header">
              <h1><?php echo $this->pluginName; ?></h1>
            </div>
          </div>
        </div>
      </section>
      <section class = "mcw-fp-settings-flex-sect">
        <div class = "mcw-fp-settings-row-parent">
          <div class = "mcw-fp-settings-cell">
            <div class = "mcw-fp-settings-content-wrapper">
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner mcw-fp-settings-border-bottom">
                <div class="mcw-fp-settings-cell mcw-fp-settings-title"><?php esc_html_e( 'Plugin Activation', $this->tag ); ?></div>
                <?php if ( $isActive ): ?>
                <div class="mcw-fp-settings-cell mcw-fp-settings-info activated">
                  <div><img src="<?php echo plugins_url( 'lib/assets/settings/correct-symbol.svg', __FILE__ ); ?>"><span class="text"><?php esc_html_e( 'Plugin Activated!', $this->tag ); ?></span></div>
                </div>
                <?php else: ?>
                <div class="mcw-fp-settings-cell mcw-fp-settings-info">
                  <div><img src="<?php echo plugins_url( 'lib/assets/settings/remove-symbol.svg', __FILE__ ); ?>"><span class="text"><?php esc_html_e( 'Plugin Not Activated!', $this->tag ); ?></span></div>
                </div>
                <?php endif; ?>
              </div>
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner">
                <div class="mcw-fp-settings-cell">
                  <div class="mcw-fp-settings-license-key-inner">
                    <img src="<?php echo plugins_url( 'lib/assets/settings/credit-card.svg', __FILE__ ); ?>" />
                    <div>
                    <?php if ( $isActive ): ?>
                      <span class="key"><?php esc_html_e( 'License', $this->tag ); ?></span>
                      <br/>
                      <?php esc_html_e( 'In order to register fullpage.js plugin for another domain, you\'ll have to deactivate your license for the current domain.', $this->tag ); ?>
                    <?php else: ?>
                      <span class="key"><?php esc_html_e( 'License Key', $this->tag ); ?></span>
                      <br/>
                      <?php esc_html_e( 'The license key was sent to you on the purchase confirmation email', $this->tag ); ?>
                    <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
              <?php if ( ! $isActive ) : ?>
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner">
                <div class="mcw-fp-settings-cell">
                  <input type="text" name="<?php echo $this->settingsId; ?>[<?php echo $this->licenseKeyId; ?>]" value="<?php echo $licenseKey; ?>" style="padding:7px;width:100%;"><br/><div class="mcw-fp-settings-note"><?php
                    if ( $licenseKey && ! $licenseState ) {
                      echo '<p class="error">' . esc_html__( 'An error occured while activating the license. Please check your license key!', $this->tag ) . '<br/>Error: ' . $this->server->GetRemoteErrorMessage() . '</p>';
                    }
                  ?></div>
                </div>
              </div>
              <?php endif; ?>
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner">
                <div class="mcw-fp-settings-cell mcw-fp-settings-<?php echo $isActive ? 'deactivate' : 'activate'; ?>">
                  <?php if ( $isActive ) {
                    submit_button( esc_html__( 'Deactivate', $this->tag ), 'delete', 'deactivate', false, null );
                  } else {
                    submit_button( esc_html__( 'Activate', $this->tag ), 'button', 'activate', false, null );
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
          <div class="mcw-fp-settings-cell">
            <div class = "mcw-fp-settings-content-wrapper">
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner mcw-fp-settings-border-bottom">
                <div class="mcw-fp-settings-cell mcw-fp-settings-title"><?php esc_html_e( 'Plugin Updates', $this->tag ); ?></div>
              </div>
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner">
                <div class="mcw-fp-settings-cell">
                  <div class="mcw-fp-settings-license-key-inner"><div><span class="key"><?php esc_html_e( 'Installed Version', $this->tag ); ?></span><br/><?php echo $this->version; ?></div></div>
                </div>
              </div>
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner">
                <div class="mcw-fp-settings-cell">
                  <div class="mcw-fp-settings-license-key-inner"><div><span class="key"><?php esc_html_e( 'Latest Available Version', $this->tag ); ?></span><br/><?php echo $isActive ? $this->server->GetRemoveVersion() : '----'; ?></div></div>
                </div>
              </div>
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner">
                <div class="mcw-fp-settings-cell">
                  <?php if ( $url ) : ?>
                    <a href="<?php echo $url; ?>" class="button"><?php esc_html_e( 'Update', $this->tag ); ?></a>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class = "mcw-fp-settings-flex-sect">
        <div class = "mcw-fp-settings-row-parent">
          <div class = "mcw-fp-settings-cell">
            <div class = "mcw-fp-settings-content-wrapper mcw-fp-settings-header">
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner mcw-fp-settings-border-bottom">
                <div class="mcw-fp-settings-cell mcw-fp-settings-title"><a href="https://www.meceware.com/docs/fullpage-for-gutenberg/" target="blank"><?php esc_html_e( 'Documentation', $this->tag ); ?></a></div>
              </div>
              <div class="mcw-fp-settings-row-block mcw-fp-settings-row-inner">
                <p>Check out the tutorial video!</p>
                <p><a href="https://www.meceware.com/docs/fullpage-for-gutenberg/#tutorial-video" target="blank"><img src="<?php echo plugins_url( 'lib/assets/settings/tutorial-video.jpg', __FILE__ ); ?>" /></a></p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <?php if ( $isActive ) : ?>
      <section class = "mcw-fp-settings-flex-sect">
        <div class = "mcw-fp-settings-row-parent">
          <div class = "mcw-fp-settings-cell">
            <div class = "mcw-fp-settings-content-wrapper mcw-fp-settings-header">
              <div class="mcw-fp-settings-row mcw-fp-settings-row-inner mcw-fp-settings-border-bottom">
                <div class="mcw-fp-settings-cell mcw-fp-settings-title"><?php esc_html_e( 'Extensions', $this->tag ); ?></div>
              </div>
              <?php foreach ( $extensions as $key => $value ) : ?>
              <div class="mcw-fp-settings-row-extension mcw-fp-settings-row-inner">
                <?php if ( $value['active'] && get_option( $value[ 'id' ] ) ): ?>
                  <?php if ( $value['key'] ) : ?>
                    <div class="mcw-fp-settings-cell mcw-fp-settings-info activated">
                      <div>
                        <img src="<?php echo plugins_url( 'lib/assets/settings/correct-symbol.svg', __FILE__ ); ?>"><span class="text"><?php echo sprintf( esc_html__( '%s Extension Plugin Activated!', $this->tag ), $value['name'] ); ?></span>
                      </div>
                    </div>
                  <?php else: ?>
                    <?php
                      $linkUrl = wp_nonce_url(
                        add_query_arg(
                          array(
                            'mcw_updates' => 1,
                            'slug' => $value[ 'id' ],
                          ),
                          admin_url( 'admin.php?page=' . $_GET[ 'page' ] )
                        ),
                        'mcw_updates'
                      );
                    ?>
                    <div class="mcw-fp-settings-cell mcw-fp-settings-info installed">
                      <div>
                        <img src="<?php echo plugins_url( 'lib/assets/settings/correct-symbol.svg', __FILE__ ); ?>"><span class="text"><?php echo sprintf( esc_html__( '%s Extension Plugin Installed!', $this->tag ), $value['name'] ); ?></span><a href="https://www.meceware.com/docs/fullpage-for-elementor/#extensions" target="_blank">(<?php esc_html_e( 'How To Activate!', $this->tag ); ?>)</a>
                      </div>
                      <a id="activate-<?php echo $value['id']; ?>" class="button button-primary" href="https://alvarotrigo.com/fullPage/extensions/activationKey.html" target="_blank"><?php esc_html_e( 'Activate Extension', $this->tag ); ?></a>
                      <a id="check-activation-<?php echo $value['id']; ?>" class="button button-primary" href="<?php echo $linkUrl; ?>"><?php esc_html_e( 'Check Activation', $this->tag ); ?></a>
                    </div>
                  <?php endif; ?>
                <?php else: ?>
                  <span class="key"><?php echo $value['name']; ?></span>
                  <input type="text" name="<?php echo $this->settingsId; ?>[<?php echo $value['id']; ?>]" value="<?php echo get_option( $value[ 'id' ] ); ?>"/>
                  <button type="button" id="install-<?php echo $value['id']; ?>" class="button button-primary fp-extension-install" data-extension="<?php echo $key; ?>">Install Extension</button>
                  <div class="mcw-fp-settings-note" style="display: none;"></div>
                <?php endif; ?>
              </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </section>
      <?php endif; ?>

      <script type="text/javascript">if (document.querySelector('.mcw-fp-settings-deactivate input.button')) document.querySelector('.mcw-fp-settings-deactivate input.button').addEventListener('click', function (e){return confirm('<?php esc_html_e( 'Are you sure you want to deactivate?', $this->tag ); ?>') || e.preventDefault();});</script>
      <?php
    }

    public function onAddOptionsPage() {
      ?>
      <form action='options.php' method='post'>
        <?php
        settings_fields( $this->tag . '-group' );
        do_settings_sections( $this->tag . '-sections' );
        ?>
      </form>
      <?php
    }

    public function onAdminNotices() {
      // Do not show in the settings page.
      if ( $this->isSettingsPage() ) {
        return;
      }

      if ( $this->getLicenseState() ) {
        return;
      }

      // Check the user transient.
      $currentUser = wp_get_current_user();
      $meta = get_user_meta( $currentUser->ID, $this->licenseUserMeta, true );
      if ( isset( $meta ) && ! empty( $meta ) && ( $meta > new DateTime( 'now' ) ) ) {
        return;
      }

      // No license is given
      $text = '';
      $url = menu_page_url( $this->tag, false );
      $button = esc_html__( 'Activate Now!', $this->tag );
      $inner = '';
      // No license is given
      if ( ! $this->getLicenseKey() ) {
        $text = sprintf( esc_html__( '%s plugin requires the license key to be activated. Please enter your license key!', $this->tag ), '<strong>' . $this->pluginName . '</strong>' );
        $inner = 'class="' . $this->tag . '-notice notice notice-info is-dismissible" data-notice="no-license"';
      } else {
        $text = sprintf( esc_html__( '%s plugin is NOT activated. Please check your license key!', $this->tag ), '<strong>' . $this->pluginName . '</strong>' );
        $inner = 'class="' . $this->tag . '-notice notice notice-error is-dismissible" data-notice="no-active-license"';
      }

      ?>
      <div <?php echo $inner; ?> style="display:flex;align-items:center;">
        <div class="mcw-fp-img-wrap" style="display:flex;align-items:center;padding:0.7em;">
          <img src="<?php echo plugins_url( 'lib/assets/logo-32.png', __FILE__ ); ?>">
        </div>
        <div class="mcw-fp-img-text">
          <p>
            <?php echo $text; ?>
          </p>
          <p><a href="<?php echo $url; ?>" class="button-primary"><?php echo $button; ?></a></p>
        </div>
      </div>
      <?php
    }

    public function onAjaxAdminNoticeRequest() {
      $notice = sanitize_text_field( $_POST['notice'] );

      check_ajax_referer( $this->tag . '-admin-notice-nonce', 'nonce' );

      if ( ( 'no-license' === $notice ) || ( 'no-active-license' === $notice ) ) {
        $currentUser = wp_get_current_user();
        update_user_meta( $currentUser->ID, $this->licenseUserMeta, ( new DateTime( 'now' ) )->modify( '+12 hours' ) );
      }

      wp_die();
    }

    public function OnAjaxFullPageExtensionInstall() {
      if ( ! wp_verify_nonce( $_POST[ 'nonce' ], $this->tag . '-fullpage-extension-install-nonce' ) ) {
        echo json_encode( array(
          'success' => false,
          'message' => esc_html__( 'The request did not pass the security check.' )
          . '<br/>'
          . esc_html__( 'Please, try again. Try to refresh the page in order to renew security nonce.' )
        ) );
        die();
      }

      $key = isset( $_POST[ 'key' ] ) ? urldecode( $_POST[ 'key' ] ) : '';
      if ( empty( $key ) || strlen( $key ) < 5 ) {
        echo json_encode( array(
          'success' => false,
          'message' => esc_html__( 'Invalid license key!' ),
        ) );
        die();
      }

      $extension = isset( $_POST[ 'extension' ] ) ? urldecode( $_POST[ 'extension' ] ) : '';
      $extensions = $this->getExtensions();
      if ( empty( $extension ) || ! array_key_exists( $extension, $extensions ) ) {
        echo json_encode( array(
          'success' => false,
          'message' => esc_html__( 'Invalid extension!' ),
        ) );
        die();
      }

      // Update option
      update_option( $extensions[ $extension ]['id'], $key );

      $remote = $this->server->GetRemoteBase( array(
        'action' => 'get_metadata',
        'slug' => $extensions[ $extension ]['id'],
        'extension' => $extension,
        'key' => $key,
        'installed_version' => '0.0.0',
        'api' => 'v2',
      ) );

      if ( ! $remote ) {
        echo json_encode( array(
          'success' => false,
          'message' => esc_html__( 'Invalid response!' ),
        ) );
        die();
      }

      $remote = json_decode( $remote['body'] );

      if ( ! isset( $remote->download_url ) || empty( $remote->download_url ) ) {
        if ( isset( $remote->license_status ) && ! $remote->license_status && isset( $remote->license_error ) && $remote->license_error ) {
          echo json_encode( array(
            'success' => false,
            'message' => esc_html__( 'Invalid response!' ) . '<br/>' . $remote->license_error,
          ) );
        } else {
          echo json_encode( array(
            'success' => false,
            'message' => esc_html__( 'Invalid response!' ) . '<br/>' . esc_html__( 'Please check your license key!' ),
          ) );
        }
        die();
      }

      $url = esc_url_raw( $remote->download_url );
      $pluginFile = download_url( $url );

      if ( is_wp_error( $pluginFile ) || ! $pluginFile ) {
        echo json_encode( array(
          'success' => false,
          'message' => esc_html__( 'Failed to download the plugin! Please check your WordPress settings or contact support!' ),
        ) );
        die();
      }

      ob_start();

      require_once( dirname( __FILE__ ) . '/lib/upgrader-skin.php' );

      $skin = new McwFullPageUpgraderSkinHelper( array() );

      add_filter( 'upgrader_package_options', function ( $options ) {
        $options['clear_destination'] = true;
        return $options;
      } );

      $upgrader = new Plugin_Upgrader( $skin );
      $upgraderResult = $upgrader->install( $pluginFile );

      $content = ob_get_clean();

      $errors = array();
      if ( $upgraderResult !== true ) {
        if ( is_object( $upgrader->skin->result ) && get_class( $upgrader->skin->result) == 'WP_Error' ) {
          if ( isset( $upgrader->skin->result->errors ) && is_array( $upgrader->skin->result->errors ) ) {
            foreach ( $upgrader->skin->result->errors as $err_id => $err ) {
              if ( is_array( $err ) ) {
                foreach ( $err as $string ) {
                  if ( is_string( $string ) ) {
                    $errors[] = $string . ( $err_id == 'folder_exists' ? ' ' . esc_html__( 'You need to uninstall the old plugin first!' ) : '');
                  }
                }
              } elseif (is_string($err)) {
                  $errors[] = $err;
              }
            }
          }
        } else {
          $errors[] = 'Unknown error while installing plugin!';
        }
      } else {
        $pluginInfo = $upgrader->skin->upgrader->plugin_info();
        if ( ! is_string( $pluginInfo ) && empty( $pluginInfo ) ) {
          $errors[] = 'Plugin installed but something went wrong!';
        }
      }

      if ( ! empty( $errors ) ) {
        $errors[] = 'Plugin installation failed.';
      } else {
        $pluginDir = trailingslashit( ABSPATH . str_replace( site_url() . '/', '', plugins_url() ) );
        if ( file_exists( $pluginDir . $pluginInfo ) ) {
          ob_start();
          activate_plugin( $pluginDir . $pluginInfo );
          $content = ob_get_clean();
        } else {
          $errors[] = 'Plugin installed but cannot be activated! Please try to activate the plugin manually from the plugins page!';
        }
      }

      @unlink( $pluginFile );

      if ( ! empty ( $errors ) ) {
        echo json_encode( array(
          'success' => false,
          'message' => implode( '<br/>', $errors )
        ) );
        die();
      }

      echo json_encode( array(
        'success' => true,
        'message' => esc_html__( 'Plugin installed successfully!' )
      ) );
      die();
    }

    // Returns if the element is enabled (1/0)
    private function getSettings( $element ) {
      if ( ! isset( $this->settings ) ) {
        $this->settings = get_option( $this->settingsId, array() );
      }

      if ( isset( $this->settings[ $element ] ) ) {
        return $this->settings[ $element ];
      }

      return null;
    }

    private function getLicenseKey() {
      return trim( $this->getSettings( $this->licenseKeyId ) );
    }

    private function getLicenseState() {
      if ( $this->server ) {
        return $this->server->GetRemoteLicense();
      } else {
        return McwFullPageCommonLocal::GetState( $this->tag );
      }
    }

    private function isSettingsPage( $screen = null ) {
      if ( ! $screen ) {
        $screen = get_current_screen();
      }

      if ( $screen && 'settings_page_' . $this->tag === $screen->base ) {
        return true;
      }

      return false;
    }

    public function getExtensions() {
      $extensions = array(
        'cards' => array(
          'name' => 'Cards',
        ),
        'continuous-horizontal' => array(
          'name' => 'Continuous Horizontal',
        ),
        'drag-and-move' => array(
          'name' => 'Drag and Move',
        ),
        'drop-effect' => array(
          'name' => 'Drop Effect',
        ),
        'fading-effect' => array(
          'name' => 'Fading Effect',
        ),
        'interlocked-slides' => array(
          'name' => 'Interlocked Slides',
        ),
        'offset-sections' => array(
          'name' => 'Offset Sections',
        ),
        'parallax' => array(
          'name' => 'Parallax',
        ),
        'reset-sliders' => array(
          'name' => 'Reset Sliders',
        ),
        'responsive-slides' => array(
          'name' => 'Responsive Slides',
        ),
        'scroll-horizontally' => array(
          'name' => 'Scroll Horizontally',
        ),
        'scroll-overflow-reset' => array(
          'name' => 'Scroll Overflow Reset',
        ),
      );

      // TODO: Array initialized for backward compatibility. With 25.10.2021, the array below can be initialized as array()
      $active = apply_filters( 'mcw-fullpage-extensions', array(
        'cards' => false,
        'continuous-horizontal' => false,
        'drag-and-move' => false,
        'drop-effect' => false,
        'fading-effect' => false,
        'interlocked-slides' => false,
        'offset-sections' => false,
        'parallax' => false,
        'reset-sliders' => false,
        'responsive-slides' => false,
        'scroll-horizontally' => false,
        'scroll-overflow-reset' => false,
      ) );

      foreach ( $extensions as $key => $value ) {
        $extensions[ $key ]['id'] = 'mcw-fullpage-extension-' . $key;
        $extensions[ $key ]['active'] = array_key_exists( $key, $active ) ? $active[ $key ] : false;
      }

      return $extensions;
    }
  }
}

new McwFullPageGutenberg();
