<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_estate' );

/** MySQL database username */
define( 'DB_USER', 'wp_estate' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wp_estate' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5C8Pa9 UB+7VE~§6Vk]&.;n[_JbaT6Y44_@ p9§YI$+no@`eZj(hb,|bTNc{zx%?');
define('SECURE_AUTH_KEY',  'i_W,|BxY%H/E!t2daAtbZ/;|tslEf%Mih!$9vvqlYwMTimnRctF8h()s0b9wIN`n');
define('LOGGED_IN_KEY',    '2Zt_$rz_3q6 Izm~7jCQD}+-N2OM9QmBsNN7e;Bn;7ltZ§&Xi@%5BvI7R=-&~NS7');
define('NONCE_KEY',        '@_hv?I{2x,PpE}xt;8e8BAjC;?0~4z3f[VEv2d§yH>TqsiIA>Lo^&YkW&qs~J=F`');
define('AUTH_SALT',        'j8Rx)dy;L)8Zu2&tdupDj:]3@I]H?CojFuImqY,?Kk88iA)Cj-VQm;l%gyWvICBA');
define('SECURE_AUTH_SALT', 'TS32tEs0{nwp+7Dn_7w99`&n[6puQ5LujJdDg@TD[,DMLQqzU0@?@6=ep+E:&lS.');
define('LOGGED_IN_SALT',   'XDn6iyUFdH-3bnLZ <jVP YZ%vCzn;D&fhqN<UX@N)GSFJ+(MRG(2EU+12)faI+s');
define('NONCE_SALT',       'tn@`%[F4.Rue{-N[2kc~yU|sMXIAGP:s@>dB} |;28§22G$0$>UheUAxM§yxO{A7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
